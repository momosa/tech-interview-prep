#!/bin/bash -ex

build() {
    local script_dir="$(dirname $0)"
    docker build \
        --tag lyx \
        --file "${script_dir}/lyx.dockerfile" \
        "${script_dir}"
}

push() {
    docker tag lyx momosa/lyx:latest
    docker push momosa/lyx:latest
}

build
push
