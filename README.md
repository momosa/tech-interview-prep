# tech-interview-prep

Some practice problems and notes for tech interviews.

- [PDF](https://momosa.gitlab.io/tech-interview-prep/tech_interview_prep.pdf)
- [Web page](https://momosa.gitlab.io/tech-interview-prep)

Created using [LyX](https://www.lyx.org/),
a `LaTeX` frontend.
That'll give us nice features like a glossary and index,
while at the same time being rather user-friendly.

The website and pdf are automatically generated with each commit.
