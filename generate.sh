#!/bin/sh

cat <<USAGE

Create an index.html and pdf file in the public/ dir.
For use with GitLab CI and GitLab pages.

USAGE

[ "$CI" ] || { echo "Only deploy using CI" 1>&2; exit 1; }

project_dir="$(dirname $0)"
source_file="$project_dir/src/tech_interview_prep.lyx"
gitlab_pages_dir="$project_dir/public"
index_html="$gitlab_pages_dir/index.html"
pdf_file="$gitlab_pages_dir/tech_interview_prep.pdf"

[ -d "$gitlab_pages_dir" ] || mkdir -p "$gitlab_pages_dir"

lyx --export-to xhtml "$index_html" "$source_file"
lyx --export-to pdf "$pdf_file" "$source_file"
